from django.shortcuts import render
from realtors.models import Realtor
from listings.models import Listing
# Create your views here.
def index(request):
	# realtors=Realtor.objects.all()
	popularListings=Listing.objects.all()
	latestListings=Listing.objects.all().order_by('-id')[:8]
	context={#context vaneko content ho
		# 'title':'Ghar Agan',#yo title display garauna ko lagi
		# 'realtors' :realtors,
		"popularListings":popularListings,
		"latestListings":latestListings,
		'title':'Ghar Agan'
	}
	return render(request,'pages/index.html',context)#context add gareko
	#yo tala ko le k k dekhaune ho vanera return garxa
def search (request):
	keyword=request.POST['keyword']
	realtors=Realtor.objects.filter(name_contains=keyword)
	context={
	'keyword':keyword,
	'realtors':realtors
	}
	return render(request,'pages/search.html',context) 