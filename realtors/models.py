from django.db import models
from datetime import datetime#date time import
# Create your models here.
class Realtor(models.Model):
	name=models.CharField(max_length=255)
	address=models.CharField(max_length=255)
	description=models.TextField(blank=True)#bolean
	is_active=models.BooleanField(default=False)#boolean
	phone=models.CharField(max_length=10)
	photo=models.ImageField(upload_to='%Y/%m/%d',blank=True)#boolean blank
	reg_date=models.DateTimeField(default=datetime.now)
	def __str__(self):#yo constructor ho
		return self.name
		