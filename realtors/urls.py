from django.urls import path, include
from . import views
urlpatterns=[
	path('',views.listRealtors, name='listRealtors'),
	path('<int:realtor>',views.viewRealtor, name='viewRealtor'),
]