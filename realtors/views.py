from django.shortcuts import render
from realtors.models import Realtor
# Create your views here.
def listRealtors(request):
	realtorsList=Realtors.objects.all()
	context={
		'realtors':realtorsList
	}
	return render(request,'realtors/realtor.html' , context)

def viewRealtor(request,realtorid):
	realtor=Realtor.objects.all(id=realtor)
	context={
		'realtor':realtor
	}
	return render(request,'realtor/realtor.html', context)